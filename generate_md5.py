import subprocess
import os 
import glob

os.chdir("assets")

for f in glob.glob("*.nx"):
    subprocess.call(f"md5sum {f} | cut -d ' ' -f 1 > {f}.md5", shell=True)